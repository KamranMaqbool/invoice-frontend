import axios from 'axios';
var cookie = require('cookie');


// const cookies = cookie.parse(req.headers.cookie ?? '');
// console.log(window.document.cookie)

const cookies = cookie.parse('access' ?? '');
console.log(cookies)
const access = cookies.access ?? false;


export const HTTP = axios.create({
  baseURL: process.env.BASE_URL,
  headers: {
    Authorization: `Bearer ${access}`
  }
})