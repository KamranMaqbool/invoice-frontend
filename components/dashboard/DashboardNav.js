import React from "react"
import { Layout, Menu, Breadcrumb } from "antd"
import Link from 'next/link'
const { Header, Content, Footer } = Layout
import { useSelector, useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import { logout } from '../../store/actions/auth';


export default function DashboardNav() {
    const dispatch = useDispatch()
    const router = useRouter()
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
    const logoutHandler = () => {
        if (dispatch && dispatch !== null && dispatch !== undefined) {
            dispatch(logout()).then(resp => {
                console.log('responsssse',resp)
                if(resp.data.status == 200){
                    router.push('/')
                }
            });
        }
    };

    return (
        <div>
            <Header>
                <div className='logo' />
                <Menu
                    theme='dark'
                    mode='horizontal'
                >
                    <Menu.Item key='1' className={router.pathname == "/dashboard" ? "ant-menu-item-selected" : ""}>
                        <Link href="/dashboard" >
                            Dashboard
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='2' className={router.pathname == "/profile" ? "ant-menu-item-selected" : ""}>
                        <Link href="/profile">
                            Profile
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='3' className={router.pathname == "/invoice" ? "ant-menu-item-selected" : ""}>
                        <Link href="/invoice">
                            Invoices
                        </Link>
                    </Menu.Item>
                    <Menu.Item key='4' onClick={logoutHandler}>
                        Logout
                    </Menu.Item>
                </Menu>
            </Header>
        </div>
    )
}
