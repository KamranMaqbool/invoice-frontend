import React, {useEffect} from "react"
import { Layout } from "antd"
import DashboardNav from "./DashboardNav"
const { Content } = Layout
import {useDispatch} from 'react-redux'
import '../../styles/invoice.module.css'
import { check_auth_status,request_refresh } from "../../store/actions/auth"

export default function dashboardLayout(props) {

    const dispatch = useDispatch()
    useEffect(() => {
        if(dispatch && dispatch !== null && dispatch !== undefined) {
            dispatch(check_auth_status()).then(resp => {
                // console.log('check_auth_status response =>',resp)
            })
            .catch(error => {
                // console.log('check auth error ',error)
            })
        }
    },[dispatch])

    return (
        <Layout>
            <DashboardNav />
            <Content style={{margin:'0px 50px', height:'100vh'}}>{props.children}</Content>
        </Layout>
    )
}
