import React, { useState } from 'react';
import { Row, Col, Modal, Button, Form, Input, Radio, Space, InputNumber, notification } from 'antd';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import axios from 'axios'

export default function Edit(props) {

    const [isLoading, setIsLoading] = useState(false);
    const [fields, setFields] = useState([
        {
            name: ["title"],
            value: props.invoiceData.title,
        },
        {
            name: ["receiver_name"],
            value: props.invoiceData.receiver_name,
        },
        {
            name: ["receiver_phone_no"],
            value: props.invoiceData.receiver_phone_no,
        },
        {
            name: ["email"],
            value: props.invoiceData.receiver_email,
        },
        {
            name: ["status"],
            value: props.invoiceData.status,
        },
        {
            name: ["received_amount"],
            value: props.invoiceData.received_amount,
        },
        {
            name: ["items"],
            value: JSON.parse(props.invoiceData.items),
        },
    ])

    const openNotification = () => {
        notification.open({
            message: 'Success',
            duration: 5,
            description:
            'Invoice updated successfully',
            onClick: () => {
            },
        });
    };

    const [isModalVisible, setIsModalVisible] = useState(true)

    const [invoiceForm] = Form.useForm()
    
    const handleCancel = () => {
        invoiceForm.resetFields()
        props.onCancel()
    }

    const onFinish = (values) => {
        
        setIsLoading(true)
        let total_amount = 0
        values.items.forEach(element => {
            let amount = element.qty*element.price
            total_amount = total_amount + amount
        });
        total_amount = total_amount.toFixed(2)

        let payload = {...values,total_amount,items:JSON.stringify(values.items),id:props.invoiceData.id}

        
        // let payload = {...values,items:JSON.stringify(values.items),id:props.invoiceData.id}
        
        axios.patch('/api/invoice/update/', payload)
        .then(resp => {
            openNotification()
            props.onCancel()
        })
        .catch(error => {
            console.log(error)
        })
        .finally(() => {
            setIsLoading(false)
        })
    }

    const onFinishFailed = (errorInfo) => {
    };

    return (
        <div>
            <Modal footer={null} width={600} title="Update Invoice" visible={isModalVisible} onCancel={handleCancel}>
                    <Form
                        name="create_invoice"
                        initialValues={{
                            remember: true,
                        }}
                        fields={fields}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        autoComplete="off"
                        form={invoiceForm}
                        layout="vertical"
                        >
                        <Form.Item
                            name="title"
                            rules={[
                            {
                                required: true,
                                message: 'Please input title!',
                            },
                            ]}
                        >
                            <Input placeholder="Title" />
                        </Form.Item>

                        <Form.Item
                            name="receiver_name"
                            rules={[
                            {
                                required: true,
                                message: 'Please input receiver name!',
                            },
                            ]}
                        >
                            <Input placeholder="Name"/>
                        </Form.Item>

                        <Form.Item
                                name="email"
                                rules={[
                                {
                                    required: true,
                                    type:'email',
                                    message: 'Please input receiver email!',
                                },
                                ]}
                            >
                                <Input placeholder="Email" />
                            </Form.Item>

                        <Form.Item
                            label="Receiver Phone Number"
                            name="receiver_phone_no"
                            rules={[
                            {
                                message: 'Please input receiver phone number!',
                            },
                            ]}
                        >
                            <Input placeholder="Phone Number" />
                        </Form.Item>
                        <Form.Item name="status">
                            <Radio.Group>
                            <Radio value="paid">Paid</Radio>
                            <Radio value="unpaid">Unpaid</Radio>
                            </Radio.Group>
                        </Form.Item>

                        <Form.List name="items">
                            {(fields, { add, remove }) => (
                            <>
                                {fields.map(({ key, name, fieldKey, ...restField }) => (
                                    <Space key={key} style={{marginBottom: 8 }} align="baseline">
                                        
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'name']}
                                            fieldKey={[fieldKey, 'name']}
                                            rules={[{ required: true, message: 'Missing item name' }]}
                                            >
                                            <Input placeholder="Item Name" />
                                        </Form.Item>
                                            
                                        <Form.Item
                                            {...restField}
                                            name={[name, 'qty']}
                                            fieldKey={[fieldKey, 'qty']}
                                            rules={[{ required: true, message: 'Missing Quantity' }]}
                                            >
                                            <InputNumber placeholder="Quantity" />
                                        </Form.Item>

                                        <Form.Item
                                            {...restField}
                                            name={[name, 'price']}
                                            fieldKey={[fieldKey, 'price']}
                                            rules={[{ required: true, message: 'Missing price' }]}
                                            >
                                            <InputNumber placeholder="Price" />
                                        </Form.Item>
                                            
                                        <MinusCircleOutlined onClick={() => remove(name)} />
                                    </Space>
                                ))}
                                <Form.Item style={{ display: 'flex', justifyContent:'flex-end' }}>
                                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                        Add Item
                                    </Button>
                                </Form.Item>
                            </>
                            )}
                        </Form.List>

                        <Form.Item
                            name="received_amount"
                            rules={[
                            {
                                required: true,
                                message: 'Amount Received!',
                            },
                            ]}
                        >
                            <Input placeholder="Received Amount" />
                        </Form.Item>

                        <Form.Item>
                            <Button loading={isLoading} block type="primary" htmlType="submit">
                            Update
                            </Button>
                        </Form.Item>
                        </Form>
                    </Modal>
        </div>
    )
}
