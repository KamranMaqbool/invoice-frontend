import React from 'react'
import { Row, Col,Modal, Button,Form, Input,Radio,Space,InputNumber,notification } from 'antd';

export default function AddInvoice() {
    const onFinish = (values) => {

        let total_amount = 0
        values.items.forEach(element => {
            let amount = element.qty*element.price
            total_amount = total_amount + amount
        });
        total_amount = total_amount.toFixed(2)
        let payload = {...values,total_amount}

        axios.post(`/api/invoice/add/`,payload)
        .then(resp => {
            handleCancel()
            openNotification()
            setReloadList(!reloadList)
        })
        .catch(error => {
            console.error(error.response)
        })

    }

    const handleCancel = () => {
    };

    const onFinishFailed = (errorInfo) => {
    };
    return (
        <div>
            <Modal footer={null} width={600} title="Create Invoice" visible={true} onCancel={handleCancel}>
                        <Form
                            name="create_invoice"
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete="off"
                            form={invoiceForm}
                            layout="vertical"
                            >
                            <Form.Item
                                name="title"
                                rules={[
                                {
                                    required: true,
                                    message: 'Please input title!',
                                },
                                ]}
                            >
                                <Input placeholder="Title" />
                            </Form.Item>

                            <Form.Item
                                name="receiver_name"
                                rules={[
                                {
                                    message: 'Please input receiver name!',
                                },
                                ]}
                            >
                                <Input placeholder="Name" />
                            </Form.Item>

                            <Form.Item
                                name="email"
                                rules={[
                                {
                                    required: true,
                                    type:'email',
                                    message: 'Please input receiver email!',
                                },
                                ]}
                            >
                                <Input placeholder="Email" />
                            </Form.Item>

                            <Form.Item
                                name="receiver_phone_no"
                                rules={[
                                {
                                    message: 'Please input receiver phone number!',
                                },
                                ]}
                            >
                                <Input placeholder="Phone Number" />
                            </Form.Item>

                            <Form.Item 
                                name="status"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select status!',
                                    }
                                ]}
                            >
                                <Radio.Group>
                                <Radio value="paid">Paid</Radio>
                                <Radio value="unpaid">Unpaid</Radio>
                                </Radio.Group>
                            </Form.Item>

                            <Form.List name="items">
                                {(fields, { add, remove }) => (
                                <>
                                    {fields.map(({ key, name, fieldKey, ...restField }) => (
                                        <Space key={key} style={{marginBottom: 8 }} align="baseline">
                                            
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'name']}
                                                fieldKey={[fieldKey, 'name']}
                                                rules={[{ required: true, message: 'Missing item name' }]}
                                                >
                                                <Input placeholder="Item Name" />
                                            </Form.Item>
                                                
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'qty']}
                                                fieldKey={[fieldKey, 'qty']}
                                                rules={[{ required: true, message: 'Missing Quantity' }]}
                                                >
                                                <InputNumber placeholder="Quantity" />
                                            </Form.Item>

                                            <Form.Item
                                                {...restField}
                                                name={[name, 'price']}
                                                fieldKey={[fieldKey, 'price']}
                                                rules={[{ required: true, message: 'Missing price' }]}
                                                >
                                                <InputNumber placeholder="Price" />
                                            </Form.Item>
                                                
                                            <MinusCircleOutlined onClick={() => remove(name)} />
                                        </Space>
                                    ))}
                                    <Form.Item style={{ display: 'flex', justifyContent:'flex-end' }}>
                                        <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                            Add Item
                                        </Button>
                                    </Form.Item>
                                </>
                                )}
                            </Form.List>

                            <Form.Item
                                name="received_amount"
                                rules={[
                                {
                                    required: true,
                                    message: 'Please input received amount!',
                                },
                                ]}
                            >
                                <Input placeholder="Received Amount" />
                            </Form.Item>

                            <Form.Item>
                                <Button block type="primary" htmlType="submit">
                                Submit
                                </Button>
                            </Form.Item>
                            </Form>
                    </Modal>
        </div>
    )
}
