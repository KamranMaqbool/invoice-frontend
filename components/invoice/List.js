import { Table,Space,Popconfirm, message,notification, Input,Radio } from 'antd'
import React, { useState, useEffect } from 'react';
import axios from 'axios'
import InvoiceView from './View'
export default function List(props) {

    var fileDownload = require('js-file-download');
    const [delId, setDelId] = useState(0)
    const [isView, setIsView] = useState(false)
    const [invoiceRow, setInvoiceRow] = useState({})

    const openNotification = (message) => {
        notification.open({
            message: 'Success',
            duration: 6,
            description:message,
            onClick: () => {
            },
        });
    };

    const confirm = (e) => {
        
        let params = { params:{ id:delId } }
        axios.delete(`/api/invoice/delete/`,params)
            .then(resp => {
                openNotification(resp.data.message)
                props.reloadData()
            })
            .catch(error => {
            console.error(error.response)
        })

    }

    const cancel = (e) => {
    }

    function downloadPdf(invoice) {
        let payload = { params: { 'invoice_id': invoice.id } }

        axios.get(`/api/invoice/download-pdf/`,payload)
            .then(resp => {
                fileDownload(resp.data.data,'invoice.pdf')
            })
            .catch(error => {
                console.error(error.response)
            })
    }

    function handleEdit(invoice) {
        props.parentCallback(invoice)
    }

    function handleView(invoice) {
        setInvoiceRow(invoice)
        setIsView(true)
    }

    const invoiceViewCancel = () => {
        setInvoiceRow({})
        setIsView(false)
    }

    const columns = [
        {
            title: "Title", 
            dataIndex: "title",
        },
        {
            title: "Status",
            dataIndex: "status",
        },
        {
            title: "From",
            key: "from",
            render: (text,record) => (
                <Space size="middle">
                    {record.is_credit_for.first_name + ' ' + record.is_credit_for.last_name}
                </Space>
            )
        },
        {
            title: "To",
            dataIndex: "receiver_name",
        },
        {
            title: 'Actions',
            key: 'action',
            render: (text, record) => (
            <Space size="middle">
                <a onClick={() => handleView(record)}>View</a>
                    <a onClick={() => handleEdit(record)}>Edit</a>
                <Popconfirm
                    title="Are you sure to delete this invoice?"
                    onConfirm={confirm}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <a href="#" onClick={() => setDelId(record.id)}>Delete</a>
                </Popconfirm>
                    <a onClick={() => downloadPdf(record) }>Download PDF</a>
            </Space>
            ),
        },
    ]

    return (
        <div>
            <Table columns={columns} loading={props.loading} dataSource={props.data} />
            {isView && <InvoiceView handleCancelView={invoiceViewCancel} invoiceData={invoiceRow} /> }
        </div>
    )
}
