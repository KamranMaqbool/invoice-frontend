import React, {useState} from "react"
import { Modal, Row, Col,Typography,Divider,Table, Space,Descriptions } from 'antd';
const { Title,Text } = Typography;

export default function view({ invoiceData,handleCancelView }) {
    console.log('invoice Data=>',invoiceData)
    const [isModalVisible, setIsModalVisible] = useState(false)
    const handleCancel = () => {
        // setIsModalVisible(false);
        handleCancelView()
    }
    const invoice_items = JSON.parse(invoiceData.items)

    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Qty',
          dataIndex: 'qty',
          key: 'qty',
        },
        {
          title: 'Price',
          key: 'price',
          render: (text,record) => (
            <Space size="middle">
                {'Rs. ' + record.price}
            </Space>
        )
        },
        {
            title: 'Total',
            key: 'total',
            render: (text,record) => (
                <Space size="middle">
                    {record.qty*record.price}
                </Space>
            )
        },
    ]

    const amount_status = invoiceData.received_amount - invoiceData.total_amount

    return (
        <div>
            <Modal
                title={invoiceData.title}
                visible={true}
                onCancel={handleCancel}
                footer={null}
                width={1000}
            >

            {/* <Row>
                <Col span{12}></Col>
                <Col span{12}></Col>
            </Row> */}

            <Descriptions title="" layout="vertical">
                <Descriptions.Item label="From" span={2}>{invoiceData.is_credit_for.first_name}</Descriptions.Item>
                <Descriptions.Item label="To">
                    {invoiceData.receiver_name}
                    <br />
                    {invoiceData.receiver_email}
                    <br />
                    {invoiceData.receiver_phone_no}
                </Descriptions.Item>
            </Descriptions>

            <Table 
                bordered={true} 
                pagination={false} 
                columns={columns} 
                dataSource={invoice_items}
                summary={pageData => {
                    console.log('PageData',pageData)
                    return (
                        <>

                          <Table.Summary.Row>
                            <Table.Summary.Cell colSpan={3}><Text strong>Total</Text></Table.Summary.Cell>
                            <Table.Summary.Cell>
                              <Text type="" strong>{invoiceData.total_amount}</Text>
                            </Table.Summary.Cell>
                          </Table.Summary.Row>

                          <Table.Summary.Row>
                            <Table.Summary.Cell colSpan={3}><Text strong>Received</Text></Table.Summary.Cell>
                            <Table.Summary.Cell>
                              <Text type="" strong>{invoiceData.received_amount}</Text>
                            </Table.Summary.Cell>
                          </Table.Summary.Row>

                          <Table.Summary.Row>
                            <Table.Summary.Cell colSpan={3}><Text strong>{amount_status >= 0 ? 'Remaining' : 'Balance Due'}</Text></Table.Summary.Cell>
                            <Table.Summary.Cell>
                              <Text type="" strong>{Math.abs(invoiceData.received_amount - invoiceData.total_amount)}</Text>
                            </Table.Summary.Cell>
                          </Table.Summary.Row>

                        </>
                      );
                }}
            />

            {/* <Descriptions  title="">
                <Descriptions.Item span={3} label="Total Due">{invoiceData.total_amount}</Descriptions.Item>
                <Descriptions.Item span={3} label="Received">{invoiceData.received_amount}</Descriptions.Item>
                {amount_status >= 0 ? 
                (
                    <Descriptions.Item label="Remaining">{invoiceData.received_amount - invoiceData.total_amount}</Descriptions.Item>
                ) :
                (
                    <Descriptions.Item label="Balance Due">{invoiceData.total_amount - invoiceData.received_amount}</Descriptions.Item>    
                )
                }
            </Descriptions> */}

            </Modal>
        </div>
    )
}
