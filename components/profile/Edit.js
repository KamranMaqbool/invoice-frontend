import React, {useState} from 'react'
import { Row, Col, Modal,Card, Button, Form, Input, Avatar, Space, InputNumber, notification } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import axios from "axios"

export default function EditProfile({userData,onClose,updateProfile}) {

    let [loading,setLoading] = useState(false)
    const [fields, setFields] = useState([
        {
            name: ["first_name"],
            value: userData.first_name,
        },
        {
            name: ["last_name"],
            value: userData.last_name,
        },
        {
            name: ["address"],
            value: userData.profile.address,
        },
        {
            name: ["phone_number"],
            value: userData.profile.phone_number,
        },
    ])

    const openNotification = () => {
        notification.open({
            message: 'Success',
            duration: 6,
            description:'Profile updated successfully',
            onClick: () => {
            console.log('Notification Clicked!');
            },
        });
    };

    const onFinish = (values) => {
        setLoading(true)
        let payload = {
            ...values,
            'profile': {
                address: values.address,
                phone_number: values.phone_number ?? 0,
            },
        }

        axios
            .patch(`/api/auth/update-profile/`,payload)
            .then((resp) => {
                updateProfile(resp.data.data)
                console.log('profile update',resp)
                openNotification()
            })
            .catch((error) => {
                console.log(error.response)
            })
            .finally(() => {
                setLoading(false)
            })
    }

    const onFinishFailed = () => {

    }

    const handleCancel = () => {
        onClose()
    }

    return (
        <div>
            <Modal visible={true} footer={null} width={800} title="Update Profile" onCancel={handleCancel}>
                <Card>
                    <Row>
                        <Col span={6} style={{padding:'1rem',display:'flex',justifyContent:'center'}}>
                            <Avatar size={130} icon={<UserOutlined />} />
                        </Col>
                        <Col span={18} style={{padding:'1rem'}}>
                            <Form
                                name='updateProfile'
                                layout="vertical"
                                fields={fields}
                                onFinish={onFinish}
                                onFinishFailed={onFinishFailed}
                                autoComplete='off'
                            >
                                <Form.Item
                                    label='First Name'
                                    name='first_name'
                                    rules={[{ message: "Please input your first name!" }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label='Last Name'
                                    name='last_name'
                                    rules={[{ message: "Please input your last name!" }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label='Address'
                                    name='address'
                                    rules={[{ message: "Please input your address!" }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item
                                    label='Phone Number'
                                    name='phone_number'
                                    rules={[{ message: "Please input your phone number!" }]}
                                >
                                    <Input />
                                </Form.Item>

                                <Form.Item>
                                    <Button loading={loading} type='primary' htmlType='submit'>
                                        Update
                                    </Button>
                                </Form.Item>
                            </Form>
                        </Col>
                    </Row>
                </Card>
            </Modal>
        </div>
    )
}
