import React from "react"
import { Layout, Menu } from 'antd';
const { Header } = Layout
import './auth.module.css'
import Link from 'next/link'
import { useRouter } from 'next/router';


export default function authNav() {
    const router = useRouter()

    return (
        <Layout className="layout">
            <Header>
                <div className="logo" />
                 <Menu theme="dark" mode="horizontal">
                    <Menu.Item key="1" className={router.pathname == "/" ? "ant-menu-item-selected" : ""}>
                        <Link href="/">
                            Home
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="2" className={router.pathname == "/register" ? "ant-menu-item-selected" : ""}>
                        <Link href="/register">
                            Register
                        </Link>
                    </Menu.Item>
                    <Menu.Item key="3" className={router.pathname == "/login" ? "ant-menu-item-selected" : ""}>
                        <Link href="/login">
                            Login
                        </Link>
                    </Menu.Item>
                </Menu>
            </Header>
        </Layout>
    )
}
