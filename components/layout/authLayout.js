import React from "react"
import { Layout, Menu } from 'antd';
const { Header } = Layout
import AuthNav from './authNav.js'
export default function authLayout(props) {
    return (
        <div>
            <AuthNav />
            <main>
                {props.children}
            </main>
        </div>
    )
}
