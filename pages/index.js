import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import AuthLayout from '../components/layout/authLayout'
import { Button, Space, DatePicker, Card } from 'antd'; 

export default function Home() {
  return (
    <div>
      <Head>
        <title>Create Next App</title>
        <meta name="description" content="Index page" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
          <AuthLayout></AuthLayout>
    </div>
  )
}
