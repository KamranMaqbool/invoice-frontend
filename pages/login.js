import React, {useState} from "react"
import { Card, Form, Input, Button, Row,notification } from "antd"
import AuthLayout from '../components/layout/authLayout'
import axios from "axios"
import { useRouter } from 'next/router'
import { useSelector, useDispatch } from 'react-redux';
import { login, reset_register_success } from '../store/actions/auth';
export default function loginPage() {

    const [isLoading, setIsLoading] = useState(false)
    const router = useRouter()
    const dispatch = useDispatch()
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated)

    const onFinish = (values) => {
        
        // axios.post(`/api/auth/login/`,values)
        // .then(resp => {
        //     openNotification(resp.data.message)
        //     router.push('/dashboard')
        // })
        // .catch(error => {
        // })
        if (dispatch && dispatch !== null && dispatch !== undefined) {
            setIsLoading(true)
            dispatch(login(values)).then(resp => {
                router.push('/dashboard')
            })
            .catch(error => {
                setIsLoading(false)
            })
            .finally(() => {
            })
        }
    }

    const openNotification = (respMessage) => {
        notification.open({
            message: 'Success',
            duration: 6,
            description:respMessage,
            onClick: () => {
            console.log('Notification Clicked!');
            },
        });
    };

    const onFinishFailed = (errorInfo) => {
    }

    if (typeof window !== undefined && isAuthenticated)
        router.push('/dashboard');

    return (
        <AuthLayout>
        <div>
            <Row justify="center">
                {/* <Col span={12}> */}
                    <Card title='Login' style={{ width: 500 }}>
                        <Form
                            name='basic'
                            labelCol={{
                                span: 20,
                            }}
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete='off'
                            layout="vertical"
                        >
                            <Form.Item
                                name='email'
                                rules={[
                                    {
                                        required: true,
                                        type:'email',
                                        message: "Please input your email!",
                                    },
                                ]}
                            >
                                <Input placeholder="Email" />
                            </Form.Item>

                            <Form.Item
                                name='password'
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your password!",
                                    },
                                ]}
                            >
                                <Input.Password placeholder="Password" />
                            </Form.Item>

                            <Form.Item>
                                <Button loading={isLoading} type='primary' htmlType='submit'>
                                    Login
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                {/* </Col> */}
            </Row>
            </div>
            </AuthLayout>
    )
}
