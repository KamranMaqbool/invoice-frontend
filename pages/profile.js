import React, { useState,useEffect } from 'react';
import { Form, Input, Button, Descriptions,Row,Col } from "antd"
import DashboardLayout from "../components/dashboard/DashboardLayout"
import ProfileUpdateModal from "../components/profile/Edit"
import axios from "axios"
import { css } from "@emotion/react";
import ClipLoader from "react-spinners/ClipLoader";

export default function profile() {
    const [user, setUser] = useState({})
    let [loading, setLoading] = useState(true);
    let [isEdit, setIsEdit] = useState(false);

    const styles = {
        wrapper:{
            background: 'white',
            padding:'2rem',
            width:'100%'
        }
    }
    
    useEffect(() => {
        setLoading(true)
        axios
        .get(`/api/auth/load-user/`)
        .then((resp) => {
            console.log('user profile',resp.data)
            setUser(resp.data.data.user)
        })
        .catch((error) => {
            console.log('user profile error response =>',error.response)
        })
        .finally(() => {
            setLoading(false)
        })
    },[]);

    const handleProfileUpdate = () => {
        setIsEdit(true)
    }

    const handleEditClose = () => {
        setIsEdit(false)
    }

    const handleUpdateProfile = (updatedData) => {
        setUser(updatedData)
        setIsEdit(false)
    }

    return (
        <DashboardLayout>

            <Row style={{marginTop:'2rem'}} justify="center">
                <Col span={20}>

                    {
                        loading ? (
                                <ClipLoader loading={loading} size={150} />

                        ) : (
                            <div style={styles.wrapper}>
                                <Descriptions 
                                    title=""
                                    extra={<Button onClick={handleProfileUpdate} type="primary">Edit</Button>}
                                    >
                                    <Descriptions.Item label="First Name">{user.first_name}</Descriptions.Item>
                                    <Descriptions.Item label="Last Name">{user.last_name}</Descriptions.Item>
                                    <Descriptions.Item label=""></Descriptions.Item>
                                    <Descriptions.Item label="Phone Number">{user.profile && user.profile.phone_number}</Descriptions.Item>
                                    <Descriptions.Item label="Address">
                                    {user.profile && user.profile.address}
                                    </Descriptions.Item>
                                </Descriptions>
                            </div>
                        )
                    }
                
                </Col>
            </Row>

            {isEdit && <ProfileUpdateModal updateProfile={handleUpdateProfile}  userData={user} onClose={handleEditClose} />}

        </DashboardLayout>
    )
}
