import {BASE_URL} from '../../../config/index'
import axios from "axios"
var cookie = require("cookie")

export default async (req, res) => {
    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }
    let payload = req.body
    axios.patch(`${BASE_URL}/api/invoice/item/${payload.id}/`,payload,headers)
    .then(resp => {
        return res.status(200).json({
            message: 'Invoice update successfully',
            data: resp.data
        })
    })
    .catch(error => {
        console.log(error.response)
    })
}