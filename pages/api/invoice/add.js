import {BASE_URL} from '../../../config/index'
import axios from "axios"
var cookie = require('cookie')

export default async (req, res) => {

    let payload = req.body

    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }
    
    axios.post(`${BASE_URL}/api/invoice/`,payload,headers)
    .then(resp => {
        return res.status(200).json({
            message: 'Invoice created successfully',
            data: resp.data
        })
    })
    .catch(error => {
        console.log(error.response)
    })
}