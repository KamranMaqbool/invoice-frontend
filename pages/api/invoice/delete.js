import {BASE_URL} from '../../../config/index'
import axios from "axios"
var cookie = require('cookie');

export default async (req, res) => {

    let {id} = req.query

    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }
    axios.delete(`${BASE_URL}/api/invoice/item/${id}/`)
    .then(resp => {
        return res.status(200).json({
            message: 'Invoice created successfully',
            data: resp.data
        })
    })
    .catch(error => {
        console.log(error.response)
    })
}