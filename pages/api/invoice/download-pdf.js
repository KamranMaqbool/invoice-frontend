import {BASE_URL} from '../../../config/index'
import axios from "axios"
var cookie = require('cookie')

export default async (req, res) => {

    let { invoice_id } = req.query
    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }
    axios.get(`${BASE_URL}/api/invoice/download-pdf/${invoice_id}`,headers)
        .then(resp => {
        return res.status(200).json({
            success: 'Invoice List',
            data: resp.data
        })
    })
    .catch(error => {
            console.log(error.response)
    })
}