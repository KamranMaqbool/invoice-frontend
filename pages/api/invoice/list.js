import {BASE_URL} from '../../../config/index'
import axios from "axios"
var cookie = require("cookie")

export default async (req, res) => {

    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }

    let {search_title,status} = req.query

    let url = `${BASE_URL}/api/invoice/`

    if (req.query.search_title)
        url += `?search=${search_title}`
    else if(req.query.status)
        url += `?status=${status}`

    axios.get(`${url}`,headers)
    .then(resp => {
        return res.status(200).json({
            success: 'Invoice List',
            data: resp.data
        })
    })
    .catch(error => {
        console.log(error.response)
    })
}