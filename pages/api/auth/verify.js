import {BASE_URL} from '../../../config/index'

var cookie = require('cookie');
import axios from "axios"

export default async (req, res) => {

    const cookies = cookie.parse(req.headers.cookie ?? '')
    const access = cookies.access ?? false
    
    if(access === false) {
        // unset authorization token in axios header
        axios.interceptors.request.use(
            config => {
                config.headers.authorization = ``;
                return config;
            },
            error => {
                return Promise.reject(error);
            }
        )
        console.log('hello')
        return res.status(403).json({
            'error':'User forbidden from making the request'
        })
    }

    axios.post(`${BASE_URL}/api/token/verify/`,{token:access})
    .then(resp => {

        if(resp.status === 200) {
            return res.status(200).json({
                success: 'token verify successfully',
                data:resp.data
            })
        }
        else {
            return res.status(resp.status).json({
                error: 'Failed to authenticate',
            })
        }
    })
    .catch(error => {
        return res.status(500).json({
            error:'something went wrong'
        })
    })
}