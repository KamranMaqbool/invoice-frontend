import {BASE_URL} from '../../../config/index'

var cookie = require('cookie');
import axios from "axios"

export default async (req, res) => {
    let data = req.body
    console.log('ENVVVVV =>>>>>>>',BASE_URL)
    axios.post(`${BASE_URL}/api/auth/register/`,data)
    .then(resp => {
        if (resp.status == 201){
            return res.status(201).json({
                message: 'user created successfully'
            })
        }
        else {
            return res.status(resp.status).json({'message':resp.message})
        }
    })
    .catch(error => {
        console.log(error)
        return res.status(500).json({
            'error': 'Something went wrong when registring for an account'
        })
    })
}