import {BASE_URL} from '../../../config/index'

var cookie = require('cookie');
import axios from "axios"

export default async (req, res) => {
    
    res.setHeader('Set-Cookie', [
        cookie.serialize(
            'access', '', {
                httpOnly: true,
                secure: process.env.NODE_ENV !== "development", 
                expires: new Date(0),
                sameSite: 'strict',
                path: '/'
            }
        ),
        cookie.serialize(
            'refresh', '', {
                httpOnly: true,
                secure: process.env.NODE_ENV !== "development",
                expires: new Date(0),
                sameSite: 'strict',
                path: '/'
            }
        )
    ])

    return res.status(200).json({
        message:'logout successfully'
    })

} 