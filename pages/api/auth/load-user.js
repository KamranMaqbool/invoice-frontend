import { BASE_URL } from "../../../config/index"
import axios from "axios"
var cookie = require("cookie")

export default async (req, res) => {
    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }

    axios
        .get(`${BASE_URL}/api/auth/load-user/`, headers)
        .then((resp) => {
            console.log("load user next api response", resp.data)
            return res.status(200).json({
                message: "user profile",
                data: resp.data,
            })
        })
        .catch((error) => {
            console.log("load user next api Error Response", error.response)
            return res.status(401).json({
                error: "user not found",
            })
        })
}
