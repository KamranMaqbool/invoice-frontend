import {BASE_URL} from '../../../config/index'

var cookie = require('cookie');
import axios from "axios"
export default async (req, res) => {
    let data = req.body
    console.log('Login Request Data =>',data)
    
    axios.post(`${BASE_URL}/api/auth/login/`,data)
        .then(resp => {
            // set access token in cookie
            res.setHeader('Set-Cookie', [
                cookie.serialize(
                    'access', resp.data.access, {
                        httpOnly: true,
                        secure: process.env.NODE_ENV !== "development", 
                        maxAge: 60*60,
                        sameSite: 'strict',
                        path: '/'
                    }
                ),
                cookie.serialize(
                    'refresh', resp.data.refresh, {
                        httpOnly: true,
                        secure: process.env.NODE_ENV !== "development",
                        maxAge: 60*60*24,
                        sameSite: 'strict',
                        path: '/'
                    }
                )
            ])
            
            // set authorization token in axios header
            // axios.interceptors.request.use(
            //     config => {
            //         config.headers.authorization = `Bearer ${resp.data.access}`;
            //         return config;
            //     },
            //     error => {
            //         return Promise.reject(error);
            //     }
            // )

            if(resp.status === 200) {
                return res.status(200).json({
                        success: 'Logged in successfully',
                        data:resp.data
                });
            }
            else {
                return res.status(resp.status).json({'error':'user not found'})
            }
        })
        .catch(error => {
            return res.status(500).json({
                error: 'Something went wrong when authenticating'
            })
        })
}