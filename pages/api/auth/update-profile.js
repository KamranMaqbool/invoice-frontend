import {BASE_URL} from '../../../config/index'

var cookie = require('cookie');
import axios from "axios"

export default async (req, res) => {

    const cookies = cookie.parse(req.headers.cookie ?? "")
    const access = cookies.access ?? false

    let headers = {
        headers: {
            Authorization: `Bearer ${access}`,
        },
    }

    let data = req.body
    axios.patch(`${BASE_URL}/api/auth/update-profile/`,data,headers)
    .then(resp => {
        return res.status(200).json({
            success: 'Profile update successfully',
            data: resp.data
        })
    })
    .catch(error => {
            console.log(error.response)
    })
}