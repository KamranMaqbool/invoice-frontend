import {BASE_URL} from '../../../config/index'

var cookie = require('cookie');
import axios from "axios"

export default async (req, res) => {

    const cookies = cookie.parse(req.headers.cookie ?? '');
    const refresh = cookies.refresh ?? false;


    if(refresh === false) {
        return res.status(401).json({
            error: 'User unauthorized to make this request'
        })
    }

    const payload = {'refresh':refresh}
    axios.post(`${BASE_URL}/api/token/refresh/`,payload)
    .then(resp => {
        if (resp.status == 200) {
            res.setHeader('Set-Cookie',[
                cookie.serialize(
                    'access',resp.data.access,{
                        httpOnly: true,
                        secure: process.env.NODE_ENV !== 'development',
                        maxAge: 60*60,
                        sameSite: 'strict',
                        path: '/'
                    }
                ),
                cookie.serialize(
                    'refresh',resp.data.refresh,{
                        httpOnly: true,
                        secure: process.env.NODE_ENV !== 'development',
                        maxAge: 60*60*24,
                        sameSite: 'strict',
                        path: '/'
                    }
                )
            ])

            // set authorization token in axios header
            axios.interceptors.request.use(
                config => {
                    config.headers.authorization = `Bearer ${resp.data.access}`;
                    return config;
                },
                error => {
                    return Promise.reject(error);
                }
            )

            res.status(200).json({
                error: "Failed to fullfill refresh request"
            })   
        }
        else {
            return res.status().json({
                error:'FAiled to fulfill refresh request'
            })
        }
    })
    .catch(error => {
        return res.status(500).json({
            'error': 'Something went wrong when trying to fulfill refresh toke'
        })
    })
}