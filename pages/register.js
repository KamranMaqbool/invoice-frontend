import React,{useState} from "react"
import { Card, Form, Input, Button, Row, Col,notification } from "antd"
import AuthLayout from '../components/layout/authLayout'
import axios from "axios"
import { useRouter } from 'next/router'
import {useDispatch} from 'react-redux'
import registerAction from '../store/actions/auth'

export default function register() {

    const [isLoading, setIsLoading] = useState(false)
    const [form] = Form.useForm()
    const router = useRouter()

    const dispatch = useDispatch()

    const onFinish = (values) => {
        setIsLoading(true)
        axios.post(`/api/auth/register/`,values)
        .then(resp => {
            router.push('/login')
            form.resetFields();
            openNotification()
        })
        .catch(error => {
            console.log(error.response)
        })
        .finally(() => {
            setIsLoading(false)
        })

        // if (dispatch && dispatch !== null && dispatch !== undefined)
        //     dispatch(registerAction(first_name, last_name, username, password, re_password));
    }

    const onFinishFailed = (errorInfo) => {
    }

    const openNotification = () => {
        notification.open({
            message: 'Success',
            duration: 6,
            description:
            'User Registerd Successfully',
            onClick: () => {
            console.log('Notification Clicked!');
            },
        });
    };

    return (
        <AuthLayout>
        <div>
            <Row justify="center">
                {/* <Col span={12}> */}
                    <Card title='Register' style={{ width: 600 }}>
                        <Form
                            name='basic'
                            labelCol={{
                                span: 20,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete='off'
                            form={form}
                            layout="vertical"
                        >
                            <Form.Item
                                name='email'
                                rules={[
                                    {
                                        type: 'email',
                                        required: true,
                                        message: "Please input your email!",
                                    },
                                ]}
                            >
                                <Input placeholder="Email" />
                            </Form.Item>

                            <Form.Item
                                name='password'
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input your password!",
                                    },
                                ]}
                            >
                                <Input.Password placeholder="Password" />
                            </Form.Item>

                            <Form.Item
                                name='confirm_password'
                                rules={[
                                    {
                                        required: true,
                                        message: "Please input confirm password!",
                                    },
                                ]}
                            >
                                <Input.Password placeholder="Confirm Password" />
                            </Form.Item>

                            <Form.Item>
                                <Button loading={isLoading} type='primary' htmlType='submit'>
                                    Register
                                </Button>
                            </Form.Item>
                        </Form>
                    </Card>
                {/* </Col> */}
            </Row>
            </div>
            </AuthLayout>
    )
}
