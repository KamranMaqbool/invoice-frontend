import { Row, Col,Modal, Button,Form, Input,Radio,Space,InputNumber,notification } from 'antd';
import DashboardLayout from "../components/dashboard/DashboardLayout"
import React, { useState,useEffect } from 'react';
import { MinusCircleOutlined, PlusOutlined } from '@ant-design/icons';
import axios from 'axios'
import List from '../components/invoice/List';
import Edit from '../components/invoice/Edit';
const { Search } = Input;


export default function invoice({invoices}) {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isEdit, setIsEdit] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [invoiceData, setInvoiceData] = useState({});
    const [initialData, setInitialData] = useState([]);
    const [reloadList, setReloadList] = useState(false);
    const [searchStatus, setSearchStatus] = React.useState('')

    const [invoiceForm] = Form.useForm()

    useEffect(() => {
        setIsLoading(true)
        axios
        .get(`/api/invoice/list/`)
        .then((resp) => {
            console.log('invoice list',resp.data.data)
            setInitialData(resp.data.data)
        })
        .catch((error) => {
            console.log(error.response)
        })
        .finally(() => {
            setIsLoading(false)
        })
    },[reloadList]);

    const openNotification = () => {
        notification.open({
            message: 'Success',
            duration: 6,
            description:
            'Invoice Created successfully',
            onClick: () => {
            },
        });
    };

    const showModal = () => {
        setIsModalVisible(true);
    }

    const handleCancel = () => {
        invoiceForm.resetFields()
        setIsModalVisible(false);
    };

    const onFinish = (values) => {
        setIsLoading(true)
        let total_amount = 0
        values.items.forEach(element => {
            let amount = element.qty*element.price
            total_amount = total_amount + amount
        });
        total_amount = total_amount.toFixed(2)
        let payload = {...values,total_amount}
        console.log('payload',payload)
        axios.post(`/api/invoice/add/`,payload)
        .then(resp => {
            handleCancel()
            openNotification()
            setReloadList(!reloadList)
        })
        .catch(error => {
            console.error(error.response)
        })
        .finally(() => {
            setIsLoading(false)
        })
    };

    const onFinishFailed = (errorInfo) => {
    };

    const handleEdit = (invoice) => {
        setInvoiceData(invoice)
        setIsEdit(true)
    }

    const handleCancelEdit = () => {
        setIsEdit(false)
        setReloadList(!reloadList)
    }

    const handleReloadList = () => {
        setReloadList(!reloadList)
    }

    const onFilterStatus = e => {
        setIsLoading(true)
        setSearchStatus(e.target.value);
        let params = {params:{status:e.target.value}}
        axios.get(`/api/invoice/list/`,params)
        .then(resp => {
            setInitialData(resp.data.data)
        })
        .catch(error => {
            console.log(error.response)
        })
        .finally(() => {
            setIsLoading(false)
        })
    };

    const onSearch = value => {
        setIsLoading(true)
        let payload = {params: {'search_title':value}}
        axios.get(`/api/invoice/list/`,payload)
        .then(resp => {
            setInitialData(resp.data.data)
        })
        .catch(error => {
            console.log(error.response)
        })
        .finally(() => {
            setIsLoading(false)
        })
    }

    return (
        <DashboardLayout>
            <div className="site-layout-content" style={{marginTop:'2rem'}}>
                <Modal footer={null} width={600} title="Create Invoice" visible={isModalVisible} onCancel={handleCancel}>
                        <Form
                            name="create_invoice"
                            initialValues={{
                                remember: true,
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            autoComplete="off"
                            form={invoiceForm}
                            layout="vertical"
                            >
                            <Form.Item
                                name="title"
                                rules={[
                                {
                                    required: true,
                                    message: 'Please input title!',
                                },
                                ]}
                            >
                                <Input placeholder="Title" />
                            </Form.Item>

                            <Form.Item
                                name="receiver_name"
                                rules={[
                                {
                                    message: 'Please input receiver name!',
                                },
                                ]}
                            >
                                <Input placeholder="Name" />
                            </Form.Item>

                            <Form.Item
                                name="receiver_email"
                                rules={[
                                {
                                    required: true,
                                    type:'email',
                                    message: 'Please input receiver email!',
                                },
                                ]}
                            >
                                <Input placeholder="Email" />
                            </Form.Item>

                            <Form.Item
                                name="receiver_phone_no"
                                rules={[
                                {
                                    message: 'Please input receiver phone number!',
                                },
                                ]}
                            >
                                <Input placeholder="Phone Number" />
                            </Form.Item>

                            <Form.Item 
                                name="status"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please select status!',
                                    }
                                ]}
                            >
                                <Radio.Group>
                                <Radio value="paid">Paid</Radio>
                                <Radio value="unpaid">Unpaid</Radio>
                                </Radio.Group>
                            </Form.Item>

                            <Form.List name="items">
                                {(fields, { add, remove }) => (
                                <>
                                    {fields.map(({ key, name, fieldKey, ...restField }) => (
                                        <Space key={key} style={{marginBottom: 8 }} align="baseline">
                                            
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'name']}
                                                fieldKey={[fieldKey, 'name']}
                                                rules={[{ required: true, message: 'Missing item name' }]}
                                                >
                                                <Input placeholder="Item Name" />
                                            </Form.Item>
                                                
                                            <Form.Item
                                                {...restField}
                                                name={[name, 'qty']}
                                                fieldKey={[fieldKey, 'qty']}
                                                rules={[{ required: true, message: 'Missing Quantity' }]}
                                                >
                                                <InputNumber placeholder="Quantity" />
                                            </Form.Item>

                                            <Form.Item
                                                {...restField}
                                                name={[name, 'price']}
                                                fieldKey={[fieldKey, 'price']}
                                                rules={[{ required: true, message: 'Missing price' }]}
                                                >
                                                <InputNumber placeholder="Price" />
                                            </Form.Item>
                                                
                                            <MinusCircleOutlined onClick={() => remove(name)} />
                                        </Space>
                                    ))}
                                    <Form.Item style={{ display: 'flex', justifyContent:'flex-end' }}>
                                        <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                                            Add Item
                                        </Button>
                                    </Form.Item>
                                </>
                                )}
                            </Form.List>

                            <Form.Item
                                name="received_amount"
                                rules={[
                                {
                                    required: true,
                                    message: 'Please input received amount!',
                                },
                                ]}
                            >
                                <Input placeholder="Received Amount" />
                            </Form.Item>

                            <Form.Item>
                                <Button loading={isLoading} block type="primary" htmlType="submit">
                                Submit
                                </Button>
                            </Form.Item>
                            </Form>
                    </Modal>


                <Row justify="end">
                    <Col>
                        <Button type="primary" onClick={showModal}>Create Invoice</Button>
                    </Col>
                </Row>

                <Row style={{margin:'0.5rem 0',padding:'2rem 0'}}>
                <Col span={12}>
                    <Search
                        style={{ width: 500 }} 
                        placeholder="search by title"
                        allowClear
                        enterButton="Search"
                        size="large"
                        onSearch={onSearch}
                        />
                </Col>
                <Col span={12}>
                    <span style={{marginRight:"1rem"}}>Status:</span> 
                    <Radio.Group onChange={onFilterStatus} value={searchStatus}>
                        <Radio value="">All</Radio>
                        <Radio value="paid">Paid</Radio>
                        <Radio value="unpaid">Unpaid</Radio>
                    </Radio.Group>
                </Col>
            </Row>
                {/* table list */}
                <List loading={isLoading} reloadData={handleReloadList} data={initialData} parentCallback={ handleEdit } />
                {isEdit && <Edit invoiceData={invoiceData} onCancel={handleCancelEdit} /> }
            </div>
        </DashboardLayout>
    )
}