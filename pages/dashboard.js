import React from 'react'
import DashboardLayout from '../components/dashboard/DashboardLayout'
import { useSelector } from 'react-redux';
import {useRouter} from 'next/router'

const dashboard = () => {

    const router  = useRouter()
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

    if (typeof window !== 'undefined' && !isAuthenticated){
        router.push('/login');
    }

    return (
        <DashboardLayout>
            <h1>Welcome to dashboard</h1>
        </DashboardLayout>
    )
}
export default dashboard