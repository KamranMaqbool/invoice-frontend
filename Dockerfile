FROM node:14.17.6-alpine3.13
# RUN addgroup app && adduser -S -G app app
# USER app
WORKDIR /app
RUN mkdir data
COPY package*.json ./
RUN npm install
COPY . .
ENV API=http://www.google.com
EXPOSE 3000
CMD ["npm","run","dev"]