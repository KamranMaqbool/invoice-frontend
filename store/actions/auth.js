import {
    REGISTER_SUCCESS,
    REGISTER_FAIL,
    RESET_REGISTER_SUCCESS,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
    LOGOUT_FAIL,
    LOAD_USER_SUCCESS,
    LOAD_USER_FAIL,
    AUTHENTICATED_SUCCESS,
    AUTHENTICATED_FAIL,
    REFRESH_SUCCESS,
    REFRESH_FAIL,
    SET_AUTH_LOADING,
    REMOVE_AUTH_LOADING,
} from './types'
import axios from 'axios'

export const reqgister = (email,password,confirm_password) => async dispatch => {

    axios.post(`/api/auth/register/`,{email,password,confirm_password})
    .then(resp => {
        dispatch({type:REGISTER_SUCCESS})
    })
    .catch(error => {
        console.log('register error response => ',error.response)
        dispatch({type:REGISTER_FAIl})
    })   
}

export const check_auth_status = () => async dispatch => {

    return axios.post(`/api/auth/verify/`)
    .then(resp => {
        dispatch({
            type: AUTHENTICATED_SUCCESS
        });
        dispatch(load_user());
        return {'data':resp}
    })
    .catch(error => {
        dispatch({
            type: AUTHENTICATED_FAIL
        });
        // res.status()
    })
};


export const logout = () => async dispatch => {

    return axios.post(`/api/auth/logout/`)
    .then(resp => {
        dispatch({
            type: LOGOUT_SUCCESS
        });
        return {'data':resp}
    })
    .catch(error => {
        console.log(error)
        dispatch({
            type: LOGOUT_FAIL
        });
    })
};

export const load_user = () => async dispatch => {

    return axios.get(`/api/auth/load-user/`)
    .then(resp => {
        console.log('Load User Store Response',resp)
        if (resp.status === 200) {
            dispatch({
                type: LOAD_USER_SUCCESS,
                payload: resp.data.data
            });
        } else {
            dispatch({
                type: LOAD_USER_FAIL
            });
        }
    })
    .catch(error => {
        console.log('Load User Error => ',error.response)
        dispatch({
            type: LOAD_USER_FAIL
        });
    })
};

export const login = (values) => async dispatch => {
    dispatch({
        type: SET_AUTH_LOADING
    });

    return axios.post(`/api/auth/login`,values)
        .then(async (resp) => {
            if (resp.status === 200) {
                await dispatch({
                    type: LOGIN_SUCCESS
                });
                dispatch(load_user());
            } 
            else {
                dispatch({
                    type: LOGIN_FAIL
                });
            }
        })
        .catch(error => {
            dispatch({
                type: LOGIN_FAIL
            });
        })

    dispatch({
        type: REMOVE_AUTH_LOADING
    });
};

export const request_refresh = () => async dispatch => {

    axios.get(`/api/auth/refresh/`)
    .then(resp => {
        if (resp.status === 200) {
            dispatch({
                type: REFRESH_SUCCESS
            });
            dispatch(check_auth_status());
        } else {
            dispatch({
                type: REFRESH_FAIL
            });
        }
    })
    .catch(error => {
        dispatch({
            type: REFRESH_FAIL
        });
    })
};